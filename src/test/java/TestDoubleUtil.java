import com.payclip.k21.DoubleUtil;
import org.junit.Assert;
import org.junit.Test;

public class TestDoubleUtil {

    @Test
    public void testTruncateToTwoDecimalPlacesWith1_001_Returns1_00() {
        double val=1.001;
        double expected = 1;
        double result = DoubleUtil.truncateToTwoDecimalPlaces(val);
        Assert.assertEquals(expected, result, 0);
    }
}
